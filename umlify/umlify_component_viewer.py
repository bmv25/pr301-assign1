from component_viewer import ComponentViewer
from extractor import Extractor
import matplotlib.pyplot as plt
from numpy import arange
from subprocess import CalledProcessError
from graphviz import ExecutableNotFound
from os import path, makedirs


class UmlifyComponentViewer(ComponentViewer):
    def __init__(self, input_path=None, output_path=None):
        """
        Testing default values
        >>> cv = UmlifyComponentViewer(".")
        >>> print(cv.input_path)
        .
        >>> print(len(cv.components) > 0)
        True
        >>> print(cv.output_path)
        output\\

        """
        super().__init__()
        self.input_path = input_path
        if output_path:
            self.output_path = output_path
        self.e = Extractor()
        self.error = ""
        if input_path:
            self.e.set_file(input_path)
        self.components = list(self.e.get_component_dictionary().values())

    def generate_class_diagram(self, output_file_name=None):
        dot = None
        if output_file_name is None:
            output_file_name = "class-diagram"
        output_file_name = self.output_path + output_file_name
        try:
            dot = self._initialise_diagram('UML Class Diagram', 'dot')
        except ExecutableNotFound:  # pragma: no cover
            self.error = "Graphviz executable not found, please check it is properly installed."
        except CalledProcessError:  # pragma: no cover
            self.error = "An unexpected error occurred"
        if (len(self.components) > 0) & (dot is not None):
            for comp in self.components:
                dot = self._add_comp_to_diagram(dot, comp)
                dot = self._add_parents_to_diagram(dot, comp)
            dot.render(output_file_name, view=False)
            self.write_dot_to_png(dot_file=output_file_name)
            self._remove_temp_files(output_file_name)
        else:
            print("unable to add components to diagram.")

    def generate_pie_charts(self):
        for comp in self.e.get_component_dictionary().keys():
            if comp != "":
                self.generate_pie_chart(comp)

    def generate_pie_chart(self, comp_name, output_file_name=None):
        if not path.exists(self.output_path):
            makedirs(self.output_path)

        # ensuring the class name passed in starts with an uppercase letter
        comp_name = comp_name[0].upper() + comp_name[1:]
        if output_file_name is None:
            output_file_name = comp_name + '-' + "pie-chart"

        output_file_name = self.output_path + output_file_name

        the_component = self.e.get_component_dictionary().get(comp_name)
        if the_component is not None:
            att_types = self._get_attribute_types(the_component)
            removable = []
            for k, v in att_types.items():
                if v == 0:
                    removable.append(k)
            for r in removable:
                att_types = self._remove_by_key(att_types, r)

            if len(att_types) > 0:
                labels = att_types.keys()
                sizes = att_types.values()
                plt.pie(sizes, labels=labels, autopct='%1.1f%%')
                plt.title("Percentage of variable data types")
                plt.axis('equal')
                plt.savefig(output_file_name+".png")
                plt.gcf().clear()

    def generate_bar_chart(self, output_file_name=None):
        if not path.exists(self.output_path):
            makedirs(self.output_path)
        if output_file_name is None:
            output_file_name = "bar-chart"
        bar_width = 0.35
        opacity = 0.8
        objects = tuple(self.e.get_component_dictionary().keys())
        if len(objects) > 0:
            no_of_attributes = []
            no_of_functions = []
            for key in objects:
                component = self.e.get_component_dictionary().get(key)
                no_of_attributes.append(len(component.get_attributes()))
                no_of_functions.append(len(component.get_functions()))
            y_pos = arange(len(objects))

            plt.bar(y_pos, no_of_attributes, bar_width, alpha=opacity, color='b', label='attributes')
            plt.bar(y_pos + bar_width, no_of_functions, bar_width, alpha=opacity, color='g', label='functions')
            plt.xticks(y_pos + bar_width, objects, rotation=90)
            plt.subplots_adjust(bottom=0.50)
            plt.ylabel('Count')
            plt.xlabel('Class Name')
            plt.title('Number of Functions and Attributes per Class')
            plt.legend()

            plt.savefig(self.output_path+output_file_name + ".png")
            plt.gcf().clear()
        else:
            print("unable to generate bar chart, no components found")


if __name__ == "__main__":
    # import doctest
    # doctest.testmod(verbose=2)

    ucv = UmlifyComponentViewer(".")
    ucv.generate_class_diagram()
    ucv.generate_pie_chart("herbivore")
    ucv.generate_pie_charts()
    ucv.generate_bar_chart()
