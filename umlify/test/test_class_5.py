class Sunflower(Plant):
    def drop_seed(self):
        pass


class Orchid(Plant):
    pass


class Plant(object):

    def __init__(self, h=10):
        self.plant_height = h

    def grow_plant(self):
        pass
