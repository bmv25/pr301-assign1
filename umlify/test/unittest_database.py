import unittest
from umlify.database import Database
import sqlite3 as sql
import os


class DatabaseUnitTest(unittest.TestCase):
    """Unittests for Database"""
    def setUp(self):
        self.db = Database()

    def test_database_created(self):
        # Arrange
        self.db.create_connection("test_case.db")

        # Act
        actual = os.path.isfile("test_case.db")

        # Assert
        self.assertTrue(actual)

    def test_table_created(self):
        # Arrange
        self.db._conn = sql.connect("test_case.db")
        self.db._cursor = self.db._conn.cursor()
        self.db.drop_table()
        self.db.create_table()

        # Act
        statement = "SELECT name FROM sqlite_master WHERE type='table'"
        self.db._cursor.execute(statement)
        result = bool(self.db._cursor.fetchone())

        # Assert
        self.assertTrue(result)

    def test_add_to_database(self):
        # Arrange
        self.db.create_connection("test_case.db")
        expected = "Class Name:Person\n\t"
        expected += "Attribute:{'name': {'str': 'new_name'}, 'age': {'str': 'new_age'}}\n\t"
        expected += "Function:['__init__']\n\t"
        expected += "Parent:[]\n"

        # Act
        self.db.insert_data('test_class_2.py')
        actual = self.db.get_specific('test_class_2.py')

        # Assert
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main(verbosity=2)
