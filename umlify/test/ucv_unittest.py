from unittest import TestCase, main, skip
from umlify_component_viewer import UmlifyComponentViewer
from extractor import Extractor
from component import Component
from os import path, rename, makedirs
from shutil import rmtree, copyfile
from graphviz import ExecutableNotFound
from subprocess import CalledProcessError


class UCVTests(TestCase):
    """Tests for component_viewer.py"""

    def setUp(self):
        if path.exists("output"):
            rmtree("output")
        self.test_file = "test_class_5.py"
        self.ucv = UmlifyComponentViewer(self.test_file)
        self.comp = Component()
        self.comp.set_name("Herbivore")
        att_dict = {'teeth': {'str': 'sharp'}, 'eyes': {'int': '2'}, 'genus': {'dict': '{}'}, 'skin': {'str': 'furry'}}
        self.comp.set_attributes(att_dict)
        self.ucv.components.append(self.comp)

    def tearDown(self):
        if path.exists("output"):
            rmtree("output")
        if path.exists("..\\temp"):
            rename("..\\temp", "..\\graphviz")
        if path.exists("..\\temp2"):
            rmtree("..\\graphviz")
            rename("..\\temp2", "..\\graphviz")

    def test_file_path(self):
        self.assertEqual(self.test_file, self.ucv.input_path)

    @skip("skipping this test as refactored class to remove this parameter")
    def test_path_provided(self):
        self.assertTrue(self.ucv.path_provided)

    def test_extractor(self):
        self.assertIsInstance(self.ucv.e, Extractor)

    def test_components_not_none(self):
        self.assertIsNotNone(self.ucv.components)

    def test_components_not_empty(self):
        self.assertIsNot(len(self.ucv.components), 0)

    def test_components_is_list(self):
        self.assertIsInstance(self.ucv.components, list)

    def test_components_initialised(self):
        empty_comp = UmlifyComponentViewer(output_path='output/')
        self.assertEqual(len(empty_comp.components), 0)

    def test_components_list_components(self):
        comps = self.ucv.components
        test_item = comps[0]
        self.assertIsInstance(test_item, Component)

    def test_get_attribute_types(self):
        expected_types = {'dict': 1, 'int': 1, 'list': 0, 'object': 0, 'str': 2, 'tuple': 0}
        att_types = self.ucv._get_attribute_types(self.comp)
        self.assertEqual(expected_types, att_types)

    def test_get_attribute_types_without_component(self):
        expected_types = {'dict': 0, 'int': 0, 'list': 0, 'object': 0, 'str': 0, 'tuple': 0}
        att_types = self.ucv._get_attribute_types(component=None)
        self.assertEqual(expected_types, att_types)

    def test_get_attribute_types_without_attributes(self):
        expected_types = {'dict': 0, 'int': 0, 'list': 0, 'object': 0, 'str': 0, 'tuple': 0}
        self.comp.set_attributes(None)
        att_types = self.ucv._get_attribute_types(self.comp)
        self.assertEqual(expected_types, att_types)

    def test_get_attribute_types_with_empty_attributes(self):
        expected_types = {'dict': 0, 'int': 0, 'list': 0, 'object': 0, 'str': 0, 'tuple': 0}
        att_dict = {'teeth': {'': 'sharp'}}
        self.comp.set_attributes(att_dict)
        att_types = self.ucv._get_attribute_types(self.comp)
        self.assertEqual(expected_types, att_types)

    def test_generate_pie_chart(self):
        self.ucv.e.componentdict[self.comp.get_name()] = self.comp
        self.ucv._get_attribute_types(self.comp)
        self.ucv.generate_pie_chart("Herbivore")
        fname = "output\\Herbivore-pie-chart.png"
        self.assertTrue(path.isfile(fname))

    def test_generate_pie_chart_without_component(self):
        self.ucv.generate_pie_chart("Hello")
        fname = "output\\Hello-pie-chart.png"
        self.assertFalse(path.isfile(fname))

    def test_generate_pie_chart_with_ouput_file_name(self):
        self.ucv.e.componentdict[self.comp.get_name()] = self.comp
        self.ucv._get_attribute_types(self.comp)
        self.ucv.generate_pie_chart("Herbivore", output_file_name='Herbivore-pie-chart')
        fname = "output\\Herbivore-pie-chart.png"
        self.assertTrue(path.isfile(fname))

    def test_generate_class_diagram(self):
        self.ucv.generate_class_diagram()
        fname = 'output\\class-diagram.png'
        self.assertTrue(path.isfile(fname))
        
    def test_generate_class_diagram_without_components(self):
        self.ucv.components = []
        self.ucv.generate_class_diagram(output_file_name='class-diagram.png')
        fname = 'output\\class-diagram.png'
        self.assertFalse(path.isfile(fname))        

    def test_generate_pie_charts(self):
        self.ucv.e.componentdict[self.comp.get_name()] = self.comp
        self.ucv._get_attribute_types(self.comp)
        fname = "output\\Herbivore-pie-chart.png"
        self.ucv.generate_pie_charts()
        self.assertTrue(path.isfile(fname))

    def test_generate_bar_chart(self):
        fname = "output\\bar-chart.png"
        self.ucv.generate_bar_chart()
        self.assertTrue(path.isfile(fname))

    def test_generate_bar_chart_with_output_path_created(self):
        fname = "output\\bar-chart.png"
        makedirs("output")
        self.ucv.generate_bar_chart()
        self.assertTrue(path.isfile(fname))

    def test_remove_temp_files(self):
        fname = 'output\\class-diagram'
        self.ucv._remove_temp_files(fname)
        self.assertFalse(path.isfile(fname))
        
    def test_generate_bar_chart_without_object(self):
        fname = "output\\bar-chart.png"
        self.ucv.e.componentdict = {}
        self.ucv.generate_bar_chart(output_file_name='bar-chart')
        self.assertFalse(path.isfile(fname))

    def test_executable_not_found_exception(self):
        if path.exists("..\\graphviz"):
            rename("..\\graphviz", "..\\temp")
        with self.assertRaises(ExecutableNotFound) as err:
            self.ucv.generate_class_diagram()
        self.assertTrue(str(err.exception), "Graphviz executable not found, please check it is properly installed.")

    def test_called_process_error_exception(self):
        if path.exists("..\\graphviz"):
            rename("..\\graphviz", "..\\temp2")
            makedirs("..\\graphviz")
            makedirs("..\\graphviz\\release")
            makedirs("..\\graphviz\\release\\bin")
            copyfile("..\\temp2\\release\\bin\\dot.exe", "..\\graphviz\\release\\bin\\dot.exe")
        with self.assertRaises(CalledProcessError):
            self.ucv.generate_class_diagram()
            self.assertTrue(self.ucv.error, "An unexpected error occurred")


if __name__ == '__main__':
    main(verbosity=2)
