from unittest import TestSuite, makeSuite, TextTestRunner
from test_extractor import MainTests
from ucv_unittest import UCVTests


def my_suite():
    theSuite = TestSuite()
    theSuite.addTest(makeSuite(UCVTests))
    theSuite.addTest(makeSuite(MainTests))

    return theSuite

if __name__ == "__main__":
    runner = TextTestRunner(verbosity=2)
    test_suite = my_suite()
    runner.run(test_suite)