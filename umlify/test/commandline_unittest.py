from unittest import main, TestCase
from umlify.commandline import CommandLine
from os import path
from shutil import rmtree


class CVTests(TestCase):

    def setUp(self):
        self.cl = CommandLine()
        if path.exists("output"):
            rmtree("output")

    def test_do_file(self):
        # Arrange
        test_file = 'test_class_5.py'
        # Act
        self.cl.do_file(test_file)
        # Assert
        self.assertEqual(self.cl.input_path, test_file)

    def test_do_directory(self):
        # Arrange
        test_file = 'test_class_5.py'
        # Act
        self.cl.do_directory(test_file)
        # Assert
        self.assertEqual(self.cl.input_path, test_file)

    def test_do_type(self):
        # Arrange
        file_type = 'png'
        # Act
        self.cl.do_type(file_type)
        # Assert
        self.assertEqual(self.cl.output_file_type, file_type)

    def test_do_run(self):
        # Arrange
        test_file = 'test_class_5.py'
        self.cl.do_file(test_file)
        # Act
        self.cl.do_run("")
        # Assert
        self.assertTrue(self.cl.run)

    def test_check_run(self):
        # Arrange
        test_file = 'test_class_5.py'
        self.cl.do_file(test_file)
        self.cl.do_run("")
        # Act
        result = self.cl._check_run()
        # Assert
        self.assertTrue(result)

    def test_check_input(self):
        # Arrange
        test_file = 'test_class_5.py'
        self.cl.do_file(test_file)
        # Act
        result = self.cl._check_input()
        # Assert
        self.assertTrue(result)

    def test_do_bar_chart(self):
        # Arrange
        test_file = 'test_class_5.py'
        test_output_file = 'bar-chart'
        fname = "output\\bar-chart.png"
        self.cl.do_file(test_file)
        self.cl.do_run("")
        # Act
        self.cl.do_bar_chart(test_output_file)
        # Assert
        self.assertTrue(path.isfile(fname))

    def test_do_pie_chart(self):
        # Arrange
        test_file = 'test_class_5.py'
        fname = "output\\Plant-pie-chart.png"
        self.cl.do_file(test_file)
        self.cl.do_run("")
        # Act
        self.cl.do_pie_chart(None)
        # Assert
        self.assertTrue(path.isfile(fname))


if __name__ == '__main__':
    main(verbosity=2)
