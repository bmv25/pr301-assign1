import shelve
from umlify.extractor import Extractor

"""This module consists of functions that serializes and de-serializes 
    the data of Component objects and storing them into a database known as a shelf. 
    The shelf is accessed by using keys, just like a dictionary. 
    The shelve module provides object persistence and object serialization by using the pickle module.
    """


class Shelf(object):
    def __init__(self, filename):
        self.selected_file = filename
        self.selected_key = filename.strip('.py')

    def write_shelf(self, shelf_name):
        """Add component data dictionary to shelf file"""
        e = Extractor()
        e._dataextraction(self.selected_file)
        dict_item = e.get_component_dictionary()

        with shelve.open(shelf_name, "c") as shelf:
            try:
                shelf[self.selected_key] = dict_item
            finally:
                str_format = "Class components of [{}] serialized and stored to file successfully"
                print(str_format.format(self.selected_file))

    def read_shelf(self, shelf_name):
        dict_items = {}

        # return value
        new_line = "\n"

        try:
            output = ""
            with shelve.open(shelf_name, 'r') as shelf:
                try:
                    dict_items = shelf[self.selected_key]
                except Exception:
                    str_format = "Components of '{}' cannot be found in the file"
                    return str_format.format(self.selected_key)

            # possible output for when reading from the file
            for class_name, class_data in dict_items.items():
                output += "Class Name: " + class_data.name + new_line
                for attr in class_data.attributes:
                    output += "Attribute: " + attr + new_line
                for func in class_data.functions:
                    output += "Function: " + func + new_line
                for parent in class_data.parents:
                    output += "Parent: " + parent.name + new_line
                output += "-"*20 + new_line
            return output
        except IOError:
            raise Exception("File Error: '%s' does not exist!" % shelf_name)

    @staticmethod
    def clear_shelf(shelf_name):
        """ Removes all Component objects stored within the file"""
        with shelve.open(shelf_name) as shelf:
                shelf.clear()

