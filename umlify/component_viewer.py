from abc import ABC
from abc import abstractmethod
from os import path, environ, pathsep, makedirs, remove
from graphviz import Digraph

from pydot import graph_from_dot_file


class ComponentViewer(ABC):

    def __init__(self, graphviz=None):

        self.dot_class_file = "class-diagram.dot"
        if graphviz is None:
            self.path_to_graphviz = "graphviz/release/bin"
        else:
            self.path_to_graphviz = graphviz
        self.output_path = "output/"
        self.setup()

    def setup(self, new_path_to_graphviz=None):
        """
        Method to setup the path to dot.exe on the executing computer. Used for both pydot or
        pyreverse pretty printing.
        :param new_path_to_graphviz: the full path to dot.exe. If no path is
        provided, assumes the graphviz source is in the working directory under graphviz and
        sets the path to that.
        """
        if new_path_to_graphviz is None:
            dir_path = path.dirname(path.realpath(__file__))
            full_path = path.join(dir_path, self.path_to_graphviz)
            new_path_to_graphviz = path.normcase(full_path)

        environ["PATH"] += pathsep + new_path_to_graphviz

    @abstractmethod
    def generate_class_diagram(self):
        pass

    @abstractmethod
    def generate_pie_charts(self):
        pass

    @abstractmethod
    def generate_pie_chart(self, class_name):
        pass

    @abstractmethod
    def generate_bar_chart(self):
        pass

    @classmethod
    def _get_attribute_types(cls, component):
        att_types = {"int": 0, "str": 0, "dict": 0, "list": 0, "tuple": 0, "object": 0}
        if component is not None:
            attributes = component.get_attributes()

            if attributes is not None:
                if len(attributes) > 0:
                    for attrib in attributes:
                        for a in attributes[attrib]:
                            if a is not '':
                                att_types[a] = att_types[a] + 1
        return att_types

    @classmethod
    def _initialise_diagram(cls, comment, diagram_format):
        dot = Digraph(comment=comment)
        dot.node_attr['shape'] = "record"
        dot.format = diagram_format
        return dot

    @classmethod
    def _add_parents_to_diagram(cls, dot, comp):
        for parent in comp.get_parents():
            dot.edge(parent.get_name(), comp.get_name())
            dot.edge_attr.update(dir="back")
            dot.edge_attr.update(arrowtail='empty')
        return dot

    @classmethod
    def _remove_temp_files(cls, output_file_name):
        if path.exists(output_file_name):
            remove(output_file_name)
        if path.exists(output_file_name + ".dot"):
            remove(output_file_name + ".dot")

    @classmethod
    def _remove_by_key(cls, dictionary, key):
        r = dict(dictionary)
        del r[key]
        return r

    @classmethod
    def _build_record_string(cls, comp_name, attributes, functions):
        record = "{"
        record += "{name} | {attribs} |{functs}".format(name=comp_name, attribs=attributes,
                                                        functs=functions)
        record += "}"
        return record

    @classmethod
    def _add_comp_to_diagram(cls, dot, comp):
        comp_name = comp.get_name()
        attributes = cls._build_attributes_string(comp.get_attributes())
        functions = cls._build_function_string(comp.get_functions())
        record = cls._build_record_string(comp_name, attributes, functions)
        dot.node(comp_name, record)
        return dot

    @classmethod
    def _build_function_string(cls, functions):
        function_string = ""
        for funct in functions:
            function_string += funct + "\\n"
        return function_string

    @classmethod
    def _build_attributes_string(cls, attributes):
        attribute_string = ""
        for attrib in attributes:
            attribute_string += attrib
            for a in attributes[attrib]:
                attribute_string += " : " + a
            attribute_string += "\\n"
        return attribute_string

    def write_dot_to_png(self, dot_file, output=None):
        """
        Method to take a dot image definition file and convert it to a png
        :param dot_file: the name of the dot file without the .dot extension
        :param output: the name of the output file without the .png extension.
                if no output_file_name provided, will produce input_file_name.png
        """
        # reference: https://stackoverflow.com/questions/5316206/converting-dot-to-png-in-python
        if dot_file is None:
            dot_file = self.dot_class_file

        if dot_file is not None:
            if not path.exists(self.output_path):
                makedirs(self.output_path)
            if output is None:
                output = dot_file
            if not output.endswith(".png"):
                if output.endswith(".", -4, -3):
                    output = output[:-3] + "png"
                else:
                    output = output + ".png"
            try:
                (graph,) = graph_from_dot_file("{file}".format(file=dot_file))
                graph.write_png("{file}".format(file=output))
                return "file: {file} successfully converted to {result}".format(file=dot_file, result=output)

            except FileNotFoundError as err:
                print("Couldn't find the files necessary to perform the conversion.")
                print("Please provide path to dot.exe and the .dot file to convert")
                print(err)
        else:
            return "No dot file provided to convert"
